import 'package:dummy/model/user.dart';
import 'package:dummy/services/sharedPreferences.dart';
import 'package:flutter/foundation.dart';

class LoginProvider with ChangeNotifier {
  User _user;

  checkLoginState() async {
    final res = await PreferenceService().getLoginStatus;
    if (res) {
      _user = User(name: "Sumit", id: 123);
    }
    notifyListeners();
  }

  set setUser(User user) {
    _user = user;
    notifyListeners();
  }

  login(String name) {
    PreferenceService().setName(name);
    PreferenceService().setLoginStatus();
    _user = User(name: name, id: 345);
    notifyListeners();
  }

  logout() async {
    PreferenceService().removeAllPreferenceData();
    _user = null;
    notifyListeners();
  }

  User get getUser => _user;
}
