import 'package:dummy/model/user.dart';
import 'package:dummy/providers/loginProvider.dart';
import 'package:dummy/services/sharedPreferences.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatelessWidget {
  final textController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Login"),
      ),
      body: Container(
        child: Column(
          children: [
            TextField(
              controller: textController,
              decoration: InputDecoration(
                hintText: 'Name',
              ),
            ),
            Builder(builder: (context) {
              return RaisedButton(
                onPressed: () {
                  Provider.of<LoginProvider>(context, listen: false)
                      .login(textController.text);
                  Scaffold.of(context).showSnackBar(
                    SnackBar(
                      content: Text("Login Successfull"),
                    ),
                  );
                  Navigator.of(context).pop();
                },
                child: Text("Login"),
              );
            })
          ],
        ),
      ),
    );
  }
}
