import 'package:dummy/providers/loginProvider.dart';
import 'package:dummy/screens/loginScreen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: LoginProvider(),
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    super.initState();
    Provider.of<LoginProvider>(context, listen: false).checkLoginState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Consumer<LoginProvider>(
              builder: (context, _loginStatus, _) {
                if (_loginStatus.getUser == null) {
                  return Text("Not Logged in");
                } else {
                  return Text(_loginStatus.getUser.name);
                }
              },
            )
          ],
        ),
      ),
      appBar: AppBar(
        title: Text("Hello World"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton(
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => LoginScreen(),
                  ),
                );
              },
              child: Text("Login"),
            ),
            Builder(builder: (context) {
              return RaisedButton(
                onPressed: () {
                  Provider.of<LoginProvider>(context, listen: false).logout();
                  Scaffold.of(context).showSnackBar(
                    SnackBar(
                      content: Text("Logout Successfull"),
                    ),
                  );
                },
                child: Text("Logout"),
              );
            })
          ],
        ),
      ),
    );
  }
}
