import 'package:shared_preferences/shared_preferences.dart';

class PreferenceService {
  String _loginStatus = "loginStatus";
  String _name = 'name';

  Future<SharedPreferences> _getSharedPrefInstance() async {
    return await SharedPreferences.getInstance();
  }

  Future<bool> get getLoginStatus async {
    final pref = await _getSharedPrefInstance();
    return pref.getBool(_loginStatus) ?? false;
  }

  Future<void> setLoginStatus() async {
    final pref = await _getSharedPrefInstance();
    pref.setBool(_loginStatus, true);
  }

  Future<bool> get getName async {
    final pref = await _getSharedPrefInstance();
    return pref.getBool(_name) ?? "";
  }

  Future<void> setName(String name) async {
    final pref = await _getSharedPrefInstance();
    pref.setBool(name, true);
  }

  Future<void> removeAllPreferenceData() async {
    final pref = await _getSharedPrefInstance();
    pref.remove(_loginStatus);
    pref.remove(_name);
  }
}
